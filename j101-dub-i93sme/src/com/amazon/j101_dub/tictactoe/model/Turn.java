package com.amazon.j101_dub.tictactoe.model;

// holds informations on the current and next turn in the game;
// maybe history

public class Turn {
	Player curPlayer; //who is currently active
	Player nextPlayer; // who will be active next
	Integer turnIndex; // incrementing value to track the number of turns played
}
